from django import views
from django.urls import path
from . import views  

urlpatterns = [
    path('', views.api_overview, name='home'),
    path('add-mahasiswa/', views.add_mahasiswa, name='add_mahasiswa'),
    path('list-mahasiswa/', views.list_mahasiswa, name='list_mahasiswa'),
    path('update-mahasiswa/<str:pk>/', views.update_mahasiswa, name='update_mahasiswa'),
    path('delete-mahasiswa/<str:pk>/', views.delete_mahasiswa, name='delete_mahasiswa'),
    path('upload-image/', views.upload_image, name='upload_image')
]