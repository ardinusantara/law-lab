from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import serializers, status
from .models import Mahasiswa
from .serializers import ImageFileSerializer, MahasiswaSerializer
from django.shortcuts import get_object_or_404
   
@api_view(['GET'])
def api_overview(request):
    api_urls = {
        'Create': '/add-mahasiswa/',
        'Read': '/list-mahasiswa/',
        'Update': '/update-mahasiswa/<int:pk>/',
        'Delete': '/delete-mahasiswa/<int:pk>/',
        'Upload Image': '/upload-image/'
    }
  
    return Response(api_urls)

@api_view(['POST'])
def add_mahasiswa(request):
    if Mahasiswa.objects.filter(**request.data):
        raise serializers.ValidationError('This data already exists')
    
    mahasiswa = MahasiswaSerializer(data=request.data)
    
    if mahasiswa.is_valid():
        mahasiswa.save()
    
    return Response(mahasiswa.data)

@api_view(['GET'])
def list_mahasiswa(request):
    list_mahasiswa = Mahasiswa.objects.all()  
    serializer = MahasiswaSerializer(list_mahasiswa, many=True)
    return Response(serializer.data)

@api_view(['PUT'])
def update_mahasiswa(request, pk):
    mahasiswa = Mahasiswa.objects.get(id=pk)
    serializer = MahasiswaSerializer(instance=mahasiswa, data=request.data)
  
    if serializer.is_valid():
        serializer.save()
    
    return Response(serializer.data)

@api_view(['DELETE'])
def delete_mahasiswa(request, pk):
    mahasiswa = Mahasiswa.objects.get(id=pk)
    mahasiswa.delete()
    
    return Response('Mahasiswa berhasil di delete!')

@api_view(['POST'])
def upload_image(request):
    image_file = ImageFileSerializer(data=request.data)
    
    if image_file.is_valid():
        image_file.save()
    
    return Response(image_file.data)