from django.db import models

class Mahasiswa(models.Model):
    nama = models.CharField(max_length=255)
    npm = models.CharField(max_length=255)
    fakultas = models.CharField(max_length=255)
    
    def __str__(self) -> str:
        return self.nama

class ImageFile(models.Model):
    img = models.ImageField()